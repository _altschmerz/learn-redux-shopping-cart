import * as actions from './actionTypes'

export function inputDeliveryAddress(userInput) {
    return{
        type: actions.INPUT_DELIVERY_ADDRESS,
        payload: {
            address: userInput
        }
    }
}

export function inputDeliveryProvider(userInput) {
    return{
        type: actions.INPUT_DELIVERY_PROVIDER,
        payload: {
            provider: userInput
        }
    }
}