import * as actions from './actionTypes'

export function addToCart(product) {
    return {
        type: actions.ADD_TO_CART,
        payload: product
    }
}

export function removeFromCart(product) {
    return {
        type: actions.REMOVE_FROM_CART,
        payload: product
    }
}

export const fetchCartProducts = () => dispatch => {
    dispatch({
        type: actions.FETCH_CART_PRODUCTS
    })    
}