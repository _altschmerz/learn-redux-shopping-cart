import * as actions from './actionTypes'

export function inputCouponCode(userInput) {
    return {
        type: actions.INPUT_COUPON_CODE,
        payload: {
            coupon_code: userInput
        }
    }
}

export function changeCouponMessage(message) {
    return {
        type: actions.CHANGE_COUPON_MESSAGE,
        payload: {
            message: message
        }
    }
}

export function changeCouponStatus(status) {
    return {
        type: actions.CHANGE_COUPON_STATUS,
        payload: {
            is_used: status
        }
    }
}