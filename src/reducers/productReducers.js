import * as actions from '../actions/actionTypes'

export default function productReducers(state = [], { type, payload }) {
  console.log(state)
  switch(type) {
    case actions.ADD_TO_CART:
      console.log(`adding item ${payload.id} to cart`)
      console.log(state)
      return [...state.slice(0, payload.id-1), payload, ...state.slice(payload.id)]

    case actions.REMOVE_FROM_CART:
      console.log(`removing item with id ${payload.id} from cart`)
      return [...state.slice(0, payload.id-1), payload, ...state.slice(payload.id)]

    case actions.FETCH_CART_PRODUCTS:
      console.log("fetching in reducer")
      return state.filter(product => product.is_in_cart)

    default:
      return state;
  }
}