import * as actions from './../actions/actionTypes'

export default function deliveryDetailsReducers(state = {}, { type, payload }) {
    switch(type) {
        case actions.INPUT_DELIVERY_ADDRESS:
            return {
                ...state,
                address: payload.address
            }

        case actions.INPUT_DELIVERY_PROVIDER:
            return {
                ...state,
                provider: payload.provider
            }

        default:
            return state
    }
}