import * as actions from '../actions/actionTypes'

export default function CouponDataReducers(state = "", { type, payload }) {
    switch(type) {
        case actions.INPUT_COUPON_CODE:
            return {
                ...state,
                code: payload.coupon_code
            }

        case actions.CHANGE_COUPON_MESSAGE:
            return {
                ...state,
                message: payload.message
            }
            
        case actions.CHANGE_COUPON_STATUS:
            return {
                ...state,
                is_used: payload.status
            }

        default:
            return state
    }
}