import { combineReducers } from "redux"
import productReducers from './productReducers'
import couponDataReducers from './couponDataReducers'
import deliveryDetailsReducers from "./deliveryDetailsReducers"

export default combineReducers({
    products: productReducers,
    coupon_data: couponDataReducers,
    delivery_details: deliveryDetailsReducers
})