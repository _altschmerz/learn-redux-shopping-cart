import React from 'react'

import styled from 'styled-components'

import ProductSummary from './productSummary'

const ProductSummaryContainer = styled.div `

`

const CheckoutProducts = props => {
    const { products_in_cart } = props

    return (
        <ProductSummaryContainer>
            {products_in_cart.map(product => (
                <ProductSummary key={product.id} product={product} />
            ))}
        </ProductSummaryContainer>
    )
}

export default CheckoutProducts