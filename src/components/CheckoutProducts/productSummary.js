import React from 'react'

import styled from 'styled-components'

const ProductContainer = styled.div `
  padding: 1rem;
  margin: 0 0 1rem 0;
  background: #fbfbfb;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const ProductName = styled.div `
  width: 60%;
`

const ProductPrice = styled.div `
  width: 15%;
`

const ProductQuantity = styled.div `
  width: 10%;
`

const ProductSubtotal = styled.div `
  width: 15%;
`


const ProductSummary = props => {
    const { product } = props

    return (
        <ProductContainer>
            <ProductName>{product.name}</ProductName>
            <ProductPrice>${product.price}</ProductPrice>
            <ProductQuantity>x{product.quantity}</ProductQuantity>
            <ProductSubtotal>${product.subtotal}</ProductSubtotal>
        </ProductContainer>
    )
}

export default ProductSummary