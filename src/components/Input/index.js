import React from 'react'

import styled from 'styled-components'

const InputContainer = styled.div `
    display: flex;
    margin-bottom: 0.5rem;
`

const Label = styled.div `
    width: 15%;
`

const InputColumn = styled.input `
    width: 85%;
`

const Input = props => {
    const { label, defaultValue, placeholder, handleChange } = props

    return(
        <InputContainer>
            <Label>{label}: </Label>
            <InputColumn defaultValue={defaultValue} placeholder={placeholder} onChange={e => handleChange(e)} />
        </InputContainer>
    )
}

export default Input