import React from 'react'

import styled from 'styled-components'

import Input from '../Input'

const CouponContainer= styled.div `
    width: 60%;
`

const CouponCodeInput = styled.input `

`

const ButtonAndMessageContainer = styled.div `
    display: flex;
    justify-content: space-between;
`

const CouponCodeButton = styled.button `
  box-shadow: none;
`

const CouponCodeMessage = styled.div `
`

const Coupon = props => {
    const { couponCode, couponMessage, handleCouponCodeInput, handleClickCouponCodeSubmit } = props

    return (
        <CouponContainer>
            <Input defaultValue={couponCode} handleChange={handleCouponCodeInput} label="Coupon code" placeholder="Insert coupon code here" />
            <ButtonAndMessageContainer>
                <CouponCodeMessage>{couponMessage}</CouponCodeMessage>
                <CouponCodeButton onClick={() => handleClickCouponCodeSubmit()}>Use coupon</CouponCodeButton>
            </ButtonAndMessageContainer>
        </CouponContainer>
    )
}

export default Coupon