import React from 'react'
import styled from 'styled-components'

import Product from './product'

const ProductListContainer = styled.div `
    width: 80vw;
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    padding: 0 1rem 0 0;
`

const ProductList = (props) => {
    const { products, handleAddToCartButton } = props
    
    return (
        <ProductListContainer>
            {products.map(product => (
                <Product key={product.id} product={product} handleAddToCartButton={handleAddToCartButton}/>
            ))}
        </ProductListContainer>
    )
}

export default ProductList