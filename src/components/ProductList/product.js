import React from 'react'
import styled from 'styled-components'

const ProductContainer = styled.div `
    padding: 1rem;
    margin: 0 0 1rem 0;
    background: #fbfbfb;
    width: 17vw;
    height: 40vh;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: center;
`

const ProductData = styled.div `
    font-size: 1rem;
`

const Button = styled.button `
    box-shadow: none;
`

const ProductImage = styled.img `
    max-width: 15vw;
    max-height: 25vh;
`

const Product = (props) => {
    const { product, handleAddToCartButton } = props

    return (
        <ProductContainer>
            <ProductImage src={product.image_url} />
            <ProductData>{product.name}</ProductData>
            <Button onClick={() => handleAddToCartButton(product)}>Add to Cart</Button>
        </ProductContainer>
    )
}

export default Product