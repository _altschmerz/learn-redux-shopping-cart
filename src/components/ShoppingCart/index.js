import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import LinkedButton from './../LinkedButton'

const ShoppingCartContainer = styled.div `
    padding: 2rem 1rem;
    background-color: #fbfbfb;
`

const CartProduct = styled.div `
    margin: 0 0.5rem 0.5rem 0;
    background-color: #fafafa;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 7.5vh;
`

const ProductName = styled.div `
    margin: 0;
    padding: 0 0.5rem 0 0;
    width: 70%;
    display: flex;
    flex-wrap: wrap;
`

const ProductQuantity = styled.div `
    margin: 0;
    width: 10%;
`

const RemoveButton = styled.button `
    box-shadow: none;
    width: 20%;
`

const ShoppingCart = (props) => {
    const { products_in_cart, handleRemoveFromCartButton } = props

    return (
        <ShoppingCartContainer>
            <h1 style={{ margin: "0 0 1rem 0" }}>Shopping Cart</h1>
            {products_in_cart.map(product => (
                <CartProduct key={product.id}>
                    <ProductName>{product.name}</ProductName>
                    <ProductQuantity>x{product.quantity}</ProductQuantity>
                    <RemoveButton onClick={() => handleRemoveFromCartButton(product)}>Remove</RemoveButton>
                </CartProduct>
            ))}
            <LinkedButton label="Checkout" path="/checkout" />
        </ShoppingCartContainer>
    )
}

export default ShoppingCart