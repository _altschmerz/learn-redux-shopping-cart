import React from 'react'

import styled from 'styled-components'

const TotalContainer = styled.div `
    width: 35%;
`

const Total = styled.div `

`

const TotalAmount = props => {
    const { products, getTotalAmount, generateDiscountDisplay } = props
    
    return (
        <TotalContainer>
            <Total>Total amount: ${getTotalAmount(products)}</Total>
            {generateDiscountDisplay(products)}
        </TotalContainer>
    )
}

export default TotalAmount