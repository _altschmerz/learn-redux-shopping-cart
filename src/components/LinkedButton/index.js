import React from 'react'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

const Button = styled(Link) `
    color: blue;
    text-decoration: none;
    background-color: #f1f1f1;
    max-width: 5rem;
    max-height: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0.3rem;

    &:hover {
        background-color: #f5f5f5;
    }

    &:visited {
        color: black;
    }
`

const LinkedButton = props => {
    const { label, path } = props

    return (
        <Button to={path}>{label}</Button>
    )
}

export default LinkedButton