import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import ProductListPage from './../containers/ProductListPage'
import CheckoutPage from './../containers/CheckoutPage'
import DeliveryPage from './../containers/DeliveryPage'

import store from './../store'

const Routes = () => {
    console.log(store.getState())

    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={ProductListPage} />
                <Route path="/checkout" component={CheckoutPage}/>
                <Route path="/delivery" component={DeliveryPage}/>
            </Switch>
        </BrowserRouter>
    )
}

export default Routes