import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'

import { addToCart, removeFromCart } from './../../actions/productActions'

import ProductList from './../../components/ProductList'
import ShoppingCart from './../../components/ShoppingCart'

const ProductPageContainer = styled.div `
  width: 92.5vw;
  margin: auto;
`

const ComponentsContainer = styled.div `
  display: flex;
  align-items: flex-start;
`

const ProductListPage = () => {
  const products = useSelector(state => state.products)

  const dispatch = useDispatch()

  function getProductsInCart(products) {
    return products.filter(product => product.is_in_cart)
  }

  function handleAddToCartButton(product) {
    if (product.is_in_cart) {
      product.quantity++
    } else {
      product.quantity = 1
      product.is_in_cart = true
    }
    product.subtotal = product.quantity*product.price
    dispatch(addToCart(product))
  }

  function handleRemoveFromCartButton(product) {
    product.is_in_cart = false
    product.quantity = 0
    product.subtotal = 0
    dispatch(removeFromCart(product))
  }

  return (
    <ProductPageContainer>
        <h1>Products List</h1>
        <ComponentsContainer>
          <ProductList products={products} handleAddToCartButton={handleAddToCartButton}/>
          <ShoppingCart products_in_cart={getProductsInCart(products)} handleRemoveFromCartButton={handleRemoveFromCartButton}/>
        </ComponentsContainer>
    </ProductPageContainer>
  )
}

export default ProductListPage