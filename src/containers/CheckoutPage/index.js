import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import styled from 'styled-components'

import { inputCouponCode, changeCouponMessage, changeCouponStatus } from '../../actions/couponDataActions'

import CheckoutProducts from './../../components/CheckoutProducts'
import Coupon from '../../components/Coupon'
import TotalAmount from './../../components/TotalAmount'
import LinkedButton from './../../components/LinkedButton'

const CheckoutPageContainer = styled.div `
  width: 92.5vw;
  margin: auto;
  display: flex;
  flex-direction: column;
`

const DiscountDisplay = styled.div `

`

const TotalAndCouponContainer = styled.div `
  display: flex;
  justify-content: space-between;
  margin: 1rem 0;
`

const CheckoutPage = () => {
  const products_in_cart = useSelector(state => state.products.filter(product => product.is_in_cart))

  const { code, message } = useSelector(state => state.coupon_data)

  const dispatch = useDispatch()
  
  function handleCouponCodeInput(e) {
    dispatch(inputCouponCode(e.target.value))
  }

  function handleClickCouponCodeSubmit() {
    if (code === "abc") {
      dispatch(changeCouponStatus(true))
      dispatch(changeCouponMessage("Coupon successfully used! You get 10% discount!"))
    } else {
      dispatch(changeCouponStatus(false))
      dispatch(changeCouponMessage("Coupon not found."))
    }
  }

  function generateDiscountDisplay(products) {
    if (message === "Coupon successfully used! You get 10% discount!") {
      return <DiscountDisplay>After discount (10%): ${getTotalAmount(products)*9/10}</DiscountDisplay>
    }
  }

  function getTotalAmount(products) {
    let total = 0
    products.map(product => total+=product.subtotal)
    return total
  }

  return (
    <CheckoutPageContainer>
        <h1>Summary</h1>
        <CheckoutProducts products_in_cart={products_in_cart} />
        <TotalAndCouponContainer>
          <TotalAmount products={products_in_cart} getTotalAmount={getTotalAmount} generateDiscountDisplay={generateDiscountDisplay}/>
          <Coupon couponCode={code} couponMessage={message} handleCouponCodeInput={handleCouponCodeInput} handleClickCouponCodeSubmit={handleClickCouponCodeSubmit} />
        </TotalAndCouponContainer>
        <LinkedButton label="Pay" path="/delivery" />
    </CheckoutPageContainer>
  )
}

export default CheckoutPage