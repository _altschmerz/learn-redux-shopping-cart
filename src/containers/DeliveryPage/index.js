import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import styled from 'styled-components'
import axios from 'axios'

import { inputDeliveryAddress, inputDeliveryProvider } from './../../actions/deliveryDetailsActions'

import Input from './../../components/Input'

const DeliveryPageContainer = styled.div `
    width: 92.5vw;
    margin: auto;
`

const PurchaseButton = styled.button `
    display: block;
    margin: auto;
    box-shadow: none;
`

const DeliveryPage = () => {
    const state = useSelector(state => state)

    console.log(state)

    const { address, provider } = useSelector(state => state.delivery_details)

    const dispatch = useDispatch()

    function handleChangeAddress(e) {
        dispatch(inputDeliveryAddress(e.target.value))
    }

    function handleChangeProvider(e) {
        dispatch(inputDeliveryProvider(e.target.value))
    }

    function handleClickPurchaseButton() {
        axios.post('https://httpbin.org/post', state)
            .then(res => console.log(res))
            .catch(err => console.log(err))
    }

    return (
        <DeliveryPageContainer>
            <h1>Delivery Details</h1>
            <Input label={"Address"} defaultValue={address} placeholder="Insert address here" handleChange={handleChangeAddress} />
            <Input label={"Delivery provider"} defaultValue={provider} placeholder="Insert delivery provider here" handleChange={handleChangeProvider}/>
            <PurchaseButton onClick={handleClickPurchaseButton()}>Purchase</PurchaseButton>
        </DeliveryPageContainer>
    )
}

export default DeliveryPage