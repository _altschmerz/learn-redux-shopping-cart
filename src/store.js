import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux';

import rootReducer from './reducers'

const initialState = {
  products: [
    {
      "id": 1,
      "name": "Web Design with HTML, CSS, JavaScript and jQuery Set",
      "price": 26.99,
      "is_in_cart": true,
      "quantity": 1,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/41T53nRtyoL._SX435_BO1,204,203,200_.jpg",
      "subtotal": 26.99,
      "is_checked": false
    },
    {
      "id": 2,
      "name": "The Art of Computer Programming, Vol. 1: Fundamental Algorithms, 3rd Edition",
      "price": 63.23,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/41eCbcQARTL._SX342_BO1,204,203,200_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 3,
      "name": "Oculus Quest All-in-one VR Gaming Headset - 128GB",
      "price": 499.00,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/71D9OsZmWxL._AC_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 4,
      "name": "AmazonBasics High-Speed 4K HDMI Cable - 6 Feet",
      "price": 6.00,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/61pBvlYVPxL._SL1500_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 5,
      "name": "Nintendo Switch Pro Controller",
      "price": 49.04,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/71w61HvlGeL._AC_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 6,
      "name": "Terminator: Dark Fate [Blu-ray]",
      "price": 21.94,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/91ZeiznhHzL._SX522_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 7,
      "name": "Corel WordPerfect Office X9 Home & Student Edition [PC Disc]",
      "price": 49.97,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/61sjhFiqzrL._SL1000_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 8,
      "name": "Bicycle Playing Cards - Poker Size",
      "price": 3.89,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/61eqaAiYE3L._SL1000_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 9,
      "name": "Monopoly Junior Board Game",
      "price": 10.79,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/81yG%2B2fLWtL._SL1480_.jpg",
      "subtotal": 0,
      "is_checked": false
    },
    {
      "id": 10,
      "name": "GoPro HERO8 Bundle (Hero8 + 64GB SD Card)",
      "price": 399.99,
      "is_in_cart": false,
      "quantity": 0,
      "image_url": "https://images-na.ssl-images-amazon.com/images/I/51UwIH2kNHL._SL1429_.jpg",
      "subtotal": 0,
      "is_checked": false
    }
  ],
  coupon_data: {
    code: "",
    message: "Got a coupon? Insert the code in the field above!",
    is_used: false
  },
  delivery_details: {
    address: "",
    provider: ""
  }
};

const middleware = [thunk]

const store = createStore(rootReducer, initialState, applyMiddleware(...middleware))

export default store